import React from "react";
import "./App.css";
import FilterableSensorTable from "./components/FilterableSensorTable";

export interface IData {
    greenHouse: string;
    measurement: string;
    isNominal: boolean;
    sensor: string;
}

function App() {


const PRODUCTS: IData[] = [
  {greenHouse: 'Tomato Hut', measurement: '36 C', isNominal: false, sensor: 'Temp'},
  {greenHouse: 'Tomato Hut', measurement: '75 %', isNominal: true, sensor: 'Humidity'},
  {greenHouse: 'Tomato Hut', measurement: '400 ppm', isNominal: true, sensor: 'CO2'},
  {greenHouse: 'Beetville', measurement: '25 C', isNominal: true, sensor: 'Temp'},
  {greenHouse: 'Beetville', measurement: '88 %', isNominal: true, sensor: 'Humidity'},
  {greenHouse: 'Beetville', measurement: '900 ppm', isNominal: false, sensor: 'CO2'}
];

  return (
    <div className="App">
      <FilterableSensorTable data={PRODUCTS}/>
    </div>
  );
}

export default App;
