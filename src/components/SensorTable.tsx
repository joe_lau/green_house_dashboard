import * as React from "react";
import GreenHouseRow from "./GreenHouseRow";
import SensorRow from "./SensorRow";
import { IData } from "../App";
import { useEffect } from "react";

export interface ISensorTableProps {
  data: IData[];
  filterText: string;
  isNominalOnly: boolean;
}

export default function SensorTable(props: ISensorTableProps) {
  const filterText = props.filterText;
  const isNominalOnly = props.isNominalOnly;

  const [rows, setRows] = React.useState([] as any[]);
  
  useEffect(() => {
    let lastCategory = "";
    const tmp_rows = [] as any[];
    props.data.forEach((data) => {
      if (data.sensor.toLowerCase().indexOf(filterText.toLowerCase()) === -1) {
        return;
      }
      if (isNominalOnly && !data.isNominal) {
        return;
      }
      if (data.greenHouse !== lastCategory) {
        tmp_rows.push(
          <GreenHouseRow
            category={data.greenHouse}
            key={data.greenHouse + tmp_rows.length}
          />
        );
      }
      tmp_rows.push(
        <SensorRow data={data} key={data.greenHouse + data.sensor} />
      );
      // setLastCategory(data.greenHouse);
      lastCategory = data.greenHouse;
    });
    setRows(tmp_rows);
  }, [filterText, isNominalOnly, props.data]);

  return (
    <table>
      <thead>
        <tr>
          <th>Sensor</th>
          <th>Measurement</th>
        </tr>
      </thead>
      <tbody>{rows}</tbody>
    </table>
  );
}
