import * as React from 'react';
import { IData } from '../App';
import SensorTable from './SensorTable';
import SearchBar from './SearchBar';

export interface IFilterableSensorTableProps {
  data: IData[];
}


export default function FilterableSensorTable (props: IFilterableSensorTableProps) {
  const [filterText, setFilterText] = React.useState('');
  const [isNominalOnly, setIsNominalOnly] = React.useState(false);
  const handleFilterTextChange = (filterText:string) => {
    setFilterText(filterText);
  }
  
  const handleIsNominalChange=(isNominalOnly2: boolean) => {
    setIsNominalOnly(isNominalOnly2)
  }

  
    return (
      <div>
        <SearchBar
          filterText={filterText}
          isNominalOnly={isNominalOnly}
          onFilterTextChange={handleFilterTextChange}
          onIsNominalChange={handleIsNominalChange}
        />
        <SensorTable
          data={props.data}
          filterText={filterText}
          isNominalOnly={isNominalOnly}
        />
      </div>
    );
}


