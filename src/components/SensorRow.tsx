import * as React from 'react';
import { IData } from '../App';




export interface ISensorRowProps {
  data: IData
}

export default function SensorRow (props: ISensorRowProps) {
  const data = props.data;
    const name = data.isNominal ?
      data.sensor :
      <span style={{color: 'red'}}>
        {data.sensor}
      </span>;

    return (
      <tr>
        <td>{name}</td>
        <td>{data.measurement}</td>
      </tr>
    );
}
