import * as React from "react";

export interface ISearchBarProps {
  filterText: string;
  isNominalOnly: boolean;
  onFilterTextChange: (e: any) => void;
  onIsNominalChange: (e: any) => void;
}

export default function SearchBar(props: ISearchBarProps) {
  const handleFilterTextChange = (e: any) => {
    props.onFilterTextChange(e.target.value);
  };

  const handleInStockChange = (e: any) => {
    props.onIsNominalChange(e.target.checked);
  };

  return (
    <form>
      <input
        type="text"
        placeholder="Search..."
        value={props.filterText}
        onChange={handleFilterTextChange}
      />
      <p>
        <input
          type="checkbox"
          checked={props.isNominalOnly}
          onChange={handleInStockChange}
        />{" "}
        Only show nominal measurements
      </p>
    </form>
  );
}
