import * as React from "react";

export interface IGreenHouseRowProps {
  category: string;
}

export default function GreenHouseRow(props: IGreenHouseRowProps) {
  const category = props.category;
  return (
    <tr>
      <th colSpan={2}>{category}</th>
    </tr>
  );
}
